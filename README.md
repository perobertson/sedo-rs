# sedo-rs

Run a command and provide environment variables from an environment section in
a 1Password item.

## Requirements

1Password's CLI tool [op](https://support.1password.com/command-line-getting-started/).
It can be installed somewhere on your `PATH` or specified via `op_path` in the config file.

## Installing

The Makefile has an install target which will create the file `~/.cargo/bin/sedo`.
You will need to make sure that `~/.cargo/bin` is on your `PATH`.

```bash
make install
```

## Usage

```txt
sedo 0.2.2
Run a command with environment variables set from values in a 1Password item.

Environment values are set based on entries in a section called "environment".


Usage: sedo [COMMAND]

Commands:
  completion  Generates completions for a given shell
  run         Run a given command in a sedo environment
  help        Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```

```text
Run a given command in a sedo environment

Usage: sedo run [OPTIONS] [COMMAND]...

Arguments:
  [COMMAND]...  The command to run

Options:
      --config <FILE>      The config file to use
  -c <COMMAND_STRING>      The command string to execute
  -e <ENVIRONMENT>         The environment to use
  -i, --interactive        Pass the -i flag to the subshell for the command
  -l, --login              Pass the -l flag to the subshell for the command
  -h, --help               Print help
```

Sample config `~/.config/sedo/conf.toml`:

```toml
[sedo]
default_env = "git"
op_path = "/usr/local/bin/op"

[[sedo.environments]]
name = "git"
sign_in_address = "my.1password.com"
[[sedo.environments.items]] # GitLab
vault_id = "1234567890asdfghjklqwertyu"
item_id  = "uytrewqlkjhgfdsa0987654321"
[[sedo.environments.items]] # GitHub
vault_id = "1234567890asdfghjklqwertyu"
item_id  = "asdfghjklqwertyuiop1234567"

[[sedo.environments]]
name = "cloudflare"
sign_in_address = "my.1password.com"
[[sedo.environments.items]]
vault_id = "1234567890asdfghjklqwertyu"
item_id = "7654321poiuytrewqlkjhgfdsa"
```
