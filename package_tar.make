dist/tar/x86_64:
	mkdir -p dist/tar/x86_64

# Task to build all target architectures in debug
.PHONY: dist-tar-debug
dist-tar-debug: dist-tar-x86_64-debug

# Task to build all target architectures
.PHONY: dist-tar
dist-tar: dist-tar-x86_64

.PHONY: dist-tar-x86_64-debug
dist-tar-x86_64-debug: dist/tar/x86_64 target/x86_64-unknown-linux-gnu/debug/sedo
	$(eval SEDO_VERSION=$(shell target/x86_64-unknown-linux-gnu/debug/sedo --version | sed 's/sedo //'))
	rm -rf dist/build/sedo-$(SEDO_VERSION)
	mkdir -p dist/build/sedo-$(SEDO_VERSION)
	cp target/x86_64-unknown-linux-gnu/debug/sedo dist/build/sedo-$(SEDO_VERSION)
	tar --create --remove-files -f dist/tar/x86_64/sedo-debug-$(SEDO_VERSION).tar -C dist/build sedo-$(SEDO_VERSION)
	xz --compress -f dist/tar/x86_64/sedo-debug-$(SEDO_VERSION).tar
	cd dist/tar/x86_64 && sha256sum sedo-debug-$(SEDO_VERSION).tar.xz > sedo-debug-$(SEDO_VERSION).tar.xz.sha256

.PHONY: dist-tar-x86_64
dist-tar-x86_64: dist/tar/x86_64 target/x86_64-unknown-linux-gnu/release/sedo
	$(eval SEDO_VERSION=$(shell target/x86_64-unknown-linux-gnu/release/sedo --version | sed 's/sedo //'))
	rm -rf dist/build/sedo-$(SEDO_VERSION)
	mkdir -p dist/build/sedo-$(SEDO_VERSION)
	cp target/x86_64-unknown-linux-gnu/release/sedo dist/build/sedo-$(SEDO_VERSION)
	tar --create --remove-files --mtime='1970-01-01' -f dist/tar/x86_64/sedo-$(SEDO_VERSION).tar -C dist/build sedo-$(SEDO_VERSION)
	xz --compress -f dist/tar/x86_64/sedo-$(SEDO_VERSION).tar
	cd dist/tar/x86_64 && sha256sum sedo-$(SEDO_VERSION).tar.xz > sedo-$(SEDO_VERSION).tar.xz.sha256
