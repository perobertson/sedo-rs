# RPMs

Use `rpmdev-newspec` to create a new spec file from a template.

The `%prep` directive is used for expansion of the archive.
The `%autosetup` can be used here iff `Source0` is specified

The `%build` directive is used when compiling code.
