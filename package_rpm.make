dist/rpm:
	mkdir -p dist/rpm

dist/rpm/sedo.spec: dist/rpm dist/rpm/sedo.spec.template package_rpm.make
	# TODO: fetch/build a changelog from somewhere
	sed "s/__VERSION__/$(SEDO_VERSION)/g; \
	s/__CHANGELOG__//g; \
	" dist/rpm/sedo.spec.template > dist/rpm/sedo.spec

# Task to build all target architectures
.PHONY: dist-rpm
dist-rpm: dist-rpm-x86_64

dist-rpm-x86_64: dist-tar-x86_64 dist/rpm/sedo.spec
	./bin/package-rpm
