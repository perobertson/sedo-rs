# Change Log

## [Unreleased]

## [0.3.0]

Added:

- Shell completion support

Breaking Change:

- Upgraded to use 1password-cli v2
- Running of commands was moved under a `run` subcommand

## [0.2.2]

Added:

- Ability to specify the path to op via `op_path` in the config file

## [0.2.1]

Fixed:

- Use empty string for environment value when the field has no value set

## [0.2.0]

Breaking Change:

- config file format changed to support fetching from multiple items

## [0.1.0]

- Initial release

[Unreleased]: https://gitlab.com/perobertson/sedo-rs/compare/v0.3.0...main
[0.3.0]: https://gitlab.com/perobertson/sedo-rs/tags/v0.3.0
[0.2.2]: https://gitlab.com/perobertson/sedo-rs/tags/v0.2.2
[0.2.1]: https://gitlab.com/perobertson/sedo-rs/tags/v0.2.1
[0.2.0]: https://gitlab.com/perobertson/sedo-rs/tags/v0.2.0
[0.1.0]: https://gitlab.com/perobertson/sedo-rs/tags/v0.1.0
