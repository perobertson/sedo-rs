use clap::Parser;
use config::ConfigSedoEnvironment;
use shell_words;
use std::collections::HashMap;
use std::env;
use std::process::{exit, Command};
use std::string::String;

mod cli;
mod config;
mod op;

/// Run the command in a `$SHELL` subshell.
/// This allows for the command to use the injected environment variables.
///
/// # Example
///
/// Shell usage:
///
/// ```bash
/// sedo -c 'echo $SEDO'
/// ```
fn run_cmd(
    env_name: &str,
    command_string: String,
    cmd_env: HashMap<String, String>,
    interactive: bool,
    login: bool,
) -> std::process::ExitStatus {
    let shell = env::var("SHELL").expect("SHELL not set for current user");
    let mut cmd = Command::new(shell);
    if interactive {
        cmd.arg("-i");
    }
    if login {
        cmd.arg("-l");
    }
    cmd.env("SEDO", "1")
        .env("SEDO_ENV", env_name)
        .envs(cmd_env)
        .arg("-c")
        .arg(command_string)
        .spawn()
        .expect("failed to run command")
        .wait()
        .expect("failed to wait for command")
}

fn build_env(env_cfg: &ConfigSedoEnvironment, op_path: Option<&str>) -> HashMap<String, String> {
    let session_token = op::signin(&env_cfg.sign_in_address, op_path).unwrap_or_else(|msg| {
        eprintln!("ERROR: {}", msg);
        exit(1);
    });

    let mut cmd_env: HashMap<String, String> = HashMap::new();
    for item in env_cfg.items.iter() {
        let item_env = op::fetch_env(&session_token, &item.item_id, &item.vault_id, op_path);
        cmd_env.extend(item_env);
    }
    cmd_env
}

/// check for command XOR command_string
fn program(command_string: &Option<String>, command: &Option<Vec<String>>) -> String {
    if let Some(command) = command {
        let mut words: Vec<&String> = Vec::new();
        command.iter().for_each(|x| words.push(x));
        shell_words::join(words)
    } else if let Some(command_string) = command_string {
        command_string.to_owned()
    } else {
        panic!("command or command_string must be set")
    }
}

fn main() {
    let args = cli::Args::parse();
    match args.command {
        Some(cmd) => {
            match cmd {
                cli::Commands::Completion(args) => cli::completion(args),
                cli::Commands::Run(args) => args.run(),
                #[allow(unreachable_patterns)]
                _ => {
                    println!("{:?}", cmd);
                    unimplemented!()
                }
            };
        }
        None => {
            // This section is when there are no commands passed in
        }
    }
}
