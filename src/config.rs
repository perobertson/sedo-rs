use std::{fs, env};

use serde::Deserialize;

#[derive(Deserialize)]
pub(crate) struct Config {
    pub(crate)  sedo: ConfigSedo,
}

#[derive(Deserialize)]
pub(crate) struct ConfigSedo {
    pub(crate) default_env: String,
    pub(crate) environments: Vec<ConfigSedoEnvironment>,
    pub(crate) op_path: Option<String>,
}

#[derive(Deserialize)]
pub(crate) struct ConfigSedoEnvironment {
    pub(crate) name: String,
    pub(crate) sign_in_address: String,
    pub(crate) items: Vec<ConfigSedoEnvironmentItem>,
}

#[derive(Deserialize)]
pub(crate) struct ConfigSedoEnvironmentItem {
    pub(crate) vault_id: String,
    pub(crate) item_id: String,
}

fn read_config(filename: &str) -> Config {
    let msg = format!("Failed to read config: {}", filename);
    let contents = fs::read_to_string(filename).expect(&msg);
    let config: Config = toml::from_str(&contents).unwrap();
    config
}

pub(crate) fn parse_cli_config(cfg_file_path: &Option<String>) -> Config {
    if let Some(cfg_file_path) = cfg_file_path {
        read_config(&cfg_file_path)
    } else {
        match env::var("HOME") {
            Ok(val) => {
                let file = format!("{}/.config/sedo/conf.toml", val);
                read_config(&file)
            }
            Err(_) => panic!("HOME not set for current user"),
        }
    }
}
