use std::collections::HashMap;
use std::io::Read;
use std::process::{Command, Stdio};

use serde::Deserialize;

#[derive(Deserialize, Debug)]
struct VaultItem {
    id: String,
    fields: Vec<VaultItemField>,
}

#[derive(Deserialize, Debug)]
struct VaultItemField {
    label: Option<String>,
    value: Option<String>,
    section: Option<VaultItemSection>,
}

#[derive(Deserialize, Debug)]
struct VaultItemSection {
    label: Option<String>,
}

pub(crate) fn signin(sign_in_address: &str, op_path: Option<&str>) -> Result<String, String> {
    let mut op_signin = match op_path {
        Some(val) => Command::new(val),
        None => Command::new("op"),
    };

    // Cannot use the integration with the desktop application
    op_signin.env("OP_BIOMETRIC_UNLOCK_ENABLED", "false");

    op_signin.args(&["signin", "--account", sign_in_address, "--raw"]);
    op_signin.stdout(Stdio::piped());

    let mut child = op_signin.spawn().expect("failed to start op");

    // wait for op to prompt for the password
    match child.try_wait() {
        Ok(Some(status)) => {
            let msg = format!("op signin exited early with status: {}", status);
            return Err(msg);
        }
        Ok(None) => {
            // This will wait for the user to input the password
            let status = child.wait().expect("ERROR: did not prompt for password");

            match status.code() {
                Some(code) => match code {
                    0 => {
                        let mut session_token = String::new();
                        child
                            .stdout
                            .expect("failed to get op signin stdout")
                            .read_to_string(&mut session_token)
                            .expect("failed to read output");
                        let token = session_token.trim_end_matches("\n");
                        return Ok(String::from(token));
                    }
                    _ => {
                        return Err("op signin failed".to_string());
                    }
                },
                None => Err("op terminated by signal".to_string()),
            }
        }
        Err(e) => {
            let msg = format!("error attempting to wait for op signin: {}", e);
            return Err(msg.to_string());
        }
    }
}

pub(crate) fn fetch_env(
    session_token: &String,
    item_id: &String,
    vault_id: &String,
    op_path: Option<&str>,
) -> HashMap<String, String> {
    let mut cmd_get_item = match op_path {
        Some(val) => Command::new(val),
        None => Command::new("op"),
    };

    // Cannot use the integration with the desktop application
    cmd_get_item.env("OP_BIOMETRIC_UNLOCK_ENABLED", "false");

    cmd_get_item.args(&[
        "item",
        "get",
        item_id,
        "--session",
        session_token,
        "--vault",
        vault_id,
        "--format",
        "json",
    ]);
    cmd_get_item.stdout(Stdio::piped());

    let child = cmd_get_item.spawn().expect("failed to start op");
    let output = child
        .wait_with_output()
        .expect("failed to wait for op item get");

    match output.status.code() {
        None => panic!("op item get terminated by signal"),
        Some(0) => {
            let item = String::from_utf8(output.stdout).expect("failed to read item stdout");
            let vault_item: VaultItem = serde_json::from_str(&item).expect("failed to parse json");
            let mut cmd_env = HashMap::new();
            for field in vault_item.fields {
                if field.section.is_none() {
                    continue;
                }
                let section = field.section.unwrap();
                if section.label.is_none() {
                    continue;
                }
                let section_label = section.label.unwrap().to_lowercase();
                if "environment" == section_label {
                    if let Some(label) = field.label {
                        if let Some(value) = field.value {
                            cmd_env.insert(label, value);
                        } else {
                            eprintln!(
                                "[WARN] value is missing on vault item {} field. Skipping",
                                vault_item.id
                            );
                        }
                    } else {
                        eprintln!(
                            "[WARN] label is missing on vault item {} field. Skipping",
                            vault_item.id
                        );
                    }
                }
            }
            return cmd_env;
        }
        Some(code) => panic!("op item get exited with status code: {}", code),
    }
}
