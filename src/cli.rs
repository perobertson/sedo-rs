use std::io;

use clap::{
    arg, builder::NonEmptyStringValueParser, command, Command, CommandFactory, Parser, Subcommand,
};
use clap_complete::{generate, Generator, Shell};

use crate::{build_env, program, run_cmd, config::parse_cli_config};

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None, arg_required_else_help = true)]
pub(crate) struct Args {
    #[command(subcommand)]
    pub command: Option<Commands>,
}

#[derive(Debug, Subcommand)]
pub(crate) enum Commands {
    Completion(Completion),
    Run(Run),
}

/// Generates completions for a given shell
#[derive(Debug, clap::Args)]
pub(crate) struct Completion {
    // The kind of shell to generate completions for
    #[arg(value_enum)]
    shell: Shell,
}
fn print_completions<G: Generator>(gen: G, cmd: &mut Command) {
    generate(gen, cmd, cmd.get_name().to_string(), &mut io::stdout());
}
/// Print completions for a given shell
pub(crate) fn completion(args: Completion) {
    let mut cmd = Args::command();
    print_completions(args.shell, &mut cmd);
}

/// Run a given command in a sedo environment
#[derive(Debug, clap::Args)]
pub(crate) struct Run {
    /// The config file to use
    #[clap(long = "config", value_name="FILE", value_parser=NonEmptyStringValueParser::new())]
    config: Option<String>,

    /// The command string to execute
    #[clap(short = 'c', group="cmd", value_parser=NonEmptyStringValueParser::new())]
    command_string: Option<String>,

    /// The environment to use
    #[clap(short = 'e', value_parser=NonEmptyStringValueParser::new())]
    environment: Option<String>,

    /// Pass the -i flag to the subshell for the command
    #[clap(short = 'i', long = "interactive")]
    interactive: bool,

    /// Pass the -l flag to the subshell for the command
    #[clap(short = 'l', long = "login")]
    login: bool,

    /// The command to run
    #[clap(group="cmd", value_parser=NonEmptyStringValueParser::new())]
    command: Option<Vec<String>>,
}
impl Run {
    pub(crate) fn run(self: &Self) {
        let cmd: String = program(&self.command_string, &self.command);
        let cfg = parse_cli_config(&self.config);

        // The sedo environment to use
        let sedo_env = match &self.environment {
            Some(env) => env,
            None => &cfg.sedo.default_env,
        }
        .clone();

        let env_cfg = cfg
            .sedo
            .environments
            .iter()
            .find(|env| sedo_env == env.name)
            .unwrap_or_else(|| {
                let msg = format!("Could not find environment '{}' in config", sedo_env);
                panic!("{}", msg);
            });

        let cmd_env = build_env(env_cfg, cfg.sedo.op_path.as_deref());

        run_cmd(&sedo_env, cmd, cmd_env, self.interactive, self.login);
    }
}
