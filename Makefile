.DEFAULT_GOAL:=build

all := $(shell find ./src -name '*')

include package_tar.make
include package_rpm.make

~/.cargo/bin/sedo: target/release/sedo
	cargo install --offline --frozen --path .

target/debug/sedo: $(all)
	cargo build --locked
	sha256sum target/debug/sedo

target/release/sedo: $(all)
	cargo build --locked --release
	sha256sum target/release/sedo

target/x86_64-unknown-linux-gnu/debug/sedo:
	cargo build --locked --target=x86_64-unknown-linux-gnu
	sha256sum target/target/x86_64-unknown-linux-gnu/debug/sedo/sedo

target/x86_64-unknown-linux-gnu/release/sedo:
	cargo build --locked --release --target=x86_64-unknown-linux-gnu
	sha256sum target/x86_64-unknown-linux-gnu/release/sedo

.PHONY: build
build: target/debug/sedo

.PHONY: build-release
build-release: target/release/sedo

.PHONY: install
install: ~/.cargo/bin/sedo

.PHONY: list-targets
list-targets:
	rustup target list

.PHONY: uninstall
uninstall:
	cargo uninstall sedo

.PHONY: clean
clean:
	rm -rf ./target
	find . -name '*.rpm' -delete
	find . -name '*.spec' -delete
	find . -name '*.tar' -delete
	find . -name '*.tar.xz' -delete
	find . -name '*.tar.xz.sha256' -delete
